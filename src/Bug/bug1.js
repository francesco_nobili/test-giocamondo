var webdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
 'browserName' : 'Chrome',
 'browser_version' : '62.0',
 'os' : 'Windows',
 'os_version' : '10',
 'resolution' : '1600x1200',
 'browserstack.user' : 'francesco257',
 'browserstack.key' : 'iRBzCSYW6JzpqxsXwMnm',
 'browserstack.local' :true,
 'project' : 'test',
 'build' : 'bug1',
 'name' : 'giocagest',
}

var ctx = {}
var boolean = false;

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3000/#/pacchetti/2')
)
.then(
    console.log('localost!')
)
.then(
    driver.sleep(1500)
)
//this code is to write into a xpath element
.then(
    driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div/div[2]/div/div/form/div/div[5]/div/div/textarea')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(5000)
)
//this code is to save!
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/button')).click()
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(10000)
)
.then(
    _ => driver.quit()
)

