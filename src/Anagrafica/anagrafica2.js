//Questo test crea una nuova anagrafica con i campi required
//e con le aggiunte di tag, tipologia utente e relazioni
//richieste da Agata
//Francesco
var webdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
    'browserName' : 'Chrome',
    'browser_version' : '62.0',
    'os' : 'Windows',
    'os_version' : '10',
    'resolution' : '1600x1200',
    'browserstack.user' : 'macbookpro2',
    'browserstack.key' : 'BaD3QMbpWgM6QPxrn6MQ',
    'browserstack.local' :true,
    'project' : 'anagrafica 2.0',
    'build' : 'Test Parziale',
    'name' : 'giocagest',
    'browserstack.networkLogs' : 'true',
    'browserstack.debug' : 'true',
    'browserstack.console' : 'errors'
}

var ctx = {}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3001/#/')
)
.then(
    console.log('starting new test')
)
.then(
    _ => driver.sleep(3000)
)
//this is to full screen 
.then(
    driver.manage().window().setRect({
        x:0,
        y:0,
        width: 1600,
        height: 1200
    })
)
.then(
    _ => driver.sleep(5000)
)
//pressing the button to create a new anagrafica
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div/div/div[1]/div[1]/div/button')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div/div/div[1]/div[1]/div/div/button[1]')).click()
)
.then(
    _ => driver.sleep(5000)
)
//input name
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[2]/div/div/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//send name
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[2]/div/div/input')).sendKeys('NomeTest')
)
.then(
    _ => driver.sleep(2000)
)
//input cognome
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[3]/div/div/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//send congnome
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[3]/div/div/input')).sendKeys('CognomeTest')
)
.then(
    _ => driver.sleep(2000)
)
//input codice-fiscale
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[4]/div/div/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//send codice-fiscale
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/div/div/div/dl/dd[4]/div/div/input')).sendKeys('AAAAAA00A00A000A')
)
.then(
    _ => driver.sleep(2000)
)
//add tags
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-2--value"]/div[1]')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-2--value"]')).click()
)
.then(
    _ => driver.sleep(2000)
)
//add tipologia utente

//this is to save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div/div/div[1]/div[1]/div/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div/div/div/div/div/div[1]/div[1]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(10000)
)
//function
.then(function() {
    if (( _=> driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[1]/div/div/div[2]/nav/div/div/span'))) !==null) {
        console.log('SUCCESSO');
    } else {
        console.log('FALLIMENTO');
    }
})
.then(
    _ => driver.sleep(5000)
)
.then(
    _ => driver.quit()
)
