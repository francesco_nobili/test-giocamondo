//Questo test crea pratica con i campi required
//Francesco
varwebdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
 'browserName' : 'Chrome',
 'browser_version' : '62.0',
 'os' : 'Windows',
 'os_version' : '10',
 'resolution' : '1600x1200',
 'browserstack.user' : 'macbookpro2',
 'browserstack.key' : 'BaD3QMbpWgM6QPxrn6MQ',
 'browserstack.local' :true,
 'project' : 'practices',
 'build' : 'Test Parziale',
 'name' : 'giocagest',
}

var ctx = {}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3000/#/pratiche')
)
.then(
    console.log('localost!')
)
.then(
    driver.sleep(3000)
)
//this is to full screen 
.then(
    driver.manage().window().setRect({
        x:0,
        y:0,
        width: 1600,
        height: 1200
    })
)
.then(
    _ => driver.sleep(500)
)

//create a new practice
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div/div[3]/div[2]/button')).click()
)
.then(
    _ => driver.findElement(webdriver.By.id('add_practice')).click()
 )
.then(
    _ => driver.sleep(2000)
)

// new practice!
.then(
    _ => driver.get('http://localhost:3000/#/pratiche/new')
)
.then(
    _ => driver.sleep(2000)
)

//package
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]/div[1]')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]/div[1]')).sendKeys('pacchetto')
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]/div[1]')).sendKeys(webdriver.Key.DOWN)
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]/div[1]')).sendKeys(webdriver.Key.ENTER)
)
.then(
    _ => driver.sleep(2000)
)
//save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div/button[1]')).click()
)
.then(
    _ => driver.sleep(4000)
)
//function
.then(function() {
    if (( _=> driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[1]/div/div/div[2]/nav/div/div/span'))) !==null) {
        console.log('SUCCESSO');
    } else {
        console.log('FALLIMENTO');
    }
})
//quit
.then(
    _ => driver.quit()
)




