//Questo test crea una nuova pratica con i campi required
//e con l'aggiunta dei servizi
//Francesco
var webdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
 'browserName' : 'Chrome',
 'browser_version' : '62.0',
 'os' : 'Windows',
 'os_version' : '10',
 'resolution' : '1600x1200',
 'browserstack.user' : 'macbookpro2',
 'browserstack.key' : 'BaD3QMbpWgM6QPxrn6MQ',
 'browserstack.local' :true,
 'project' : 'practices-with-service',
 'build' : 'Test Parziale',
 'name' : 'giocagest',
 'browserstack.networkLogs' : 'true',
 'browserstack.debug' : 'true',
 'browserstack.console' : 'errors'
}

var ctx = {}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3000/#/pratiche/new')
)
.then(
    console.log('starting new test')
)
.then(
    driver.sleep(3000)
)
//this is to full screen 
.then(
    driver.manage().window().setRect({
        x:0,
        y:0,
        width: 1600,
        height: 1200
    })
)
//code
/*
.then(
    _ => driver.sleep(2500)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[1]/div/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[1]/div/input')).sendKeys('browserstack')
)
.then(
    _ => driver.sleep(1000)
)*/
//package
/*
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]')).sendKeys('webdriver', Key.DOWN)
)
.then(
    _ => driver.sleep(1000)
)
.(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="react-select-react-select-single--value"]')).sendKeys('webdriver', Key.ENTER)
)
.then(
    _ => driver.sleep(1000)
)*/
//services
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[2]')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[2]/div[1]/div/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[2]/div[1]/div/input')).sendKeys('ser')
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[2]/div[3]/div/div[1]/div/nav/div[1]/span[1]/span/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[2]/div[3]/div/div[2]/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[1]/div/div')).click()
)
.then(
    _ => driver.sleep(1000)
)
//add-data-1
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[2]/div/div/div/div/ul/div/div[2]/div[1]/div/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[2]/div/div/div/div/ul/div/div[2]/div[1]/div/input')).sendKeys(10102019)
)
.then(
    _ => driver.sleep(1000)
)
//add-data-2
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[2]/div/div/div/div/ul/div/div[2]/div[2]/div/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[2]/div/div/div/div/ul/div/div[2]/div[2]/div/input')).sendKeys(10202019)
)
.then(
    _ => driver.sleep(1000)
)
//add-type
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[3]/div/div[2]/div/div/form/ul/div[2]/div/div/div/div/ul/div/div[3]/div[3]/div/div/div')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="menu-"]/div[2]/ul/li[2]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//switch back
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[1]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//save
.then(
    _ => driver.findElement(webdriver.By.xpath('//*[@id="root"]/div/div/div/div[2]/div[2]/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(1000) 
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div/button[1]')).click()
)
.then(
    _ => driver.sleep(4000)
)
//function
.then(function() {
    if (( _=> driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[1]/div/div/div[2]/nav/div/div/span'))) !==null) {
        console.log('SUCCESSO');
    } else {
        console.log('FALLIMENTO');
    }
})
//quit
.then(
    _ => driver.quit()
)