//Questo test crea una nuovo pacchetto con i campi required
//e sono aggiunti i problemi di periodicità che erano stati riscontrati
//fixati e in questo test, ovviamente, testati.
//Francesco
var webdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
    'browserName' : 'Chrome',
    'browser_version' : '62.0',
    'os' : 'Windows',
    'os_version' : '10',
    'resolution' : '1600x1200',
    'browserstack.user' : 'macbookpro2',
    'browserstack.key' : 'BaD3QMbpWgM6QPxrn6MQ',
    'browserstack.local' :true,
    'project' : 'package with periods',
    'build' : 'Test Definitivo',
    'name' : 'giocagest',
    'browserstack.networkLogs' : 'true',
    'browserstack.debug' : 'true',
    'browserstack.console' : 'errors'
}

var ctx = {}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3000/#/pacchetti/2')
)
.then(
    console.log('starting new test')
)
.then(
    _ => driver.sleep(3000)
)
//this is to full screen 
.then(
    driver.manage().window().setRect({
        x:0,
        y:0,
        width: 1600,
        height: 1200
    })
)
.then(
    _ => driver.sleep(5000)
)
/*
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div/div[1]/div[1]/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div/div[1]/div[1]/input')).sendKeys('PacchettoVenditaTest2')
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div/div[3]/div/div[1]/table/tbody/tr/td[2]/a')).click()
)
.then(
    _ => driver.sleep(2000)
)
*/
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/header/div/div[2]/div[2]/div/button[3]')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
//this is to add an extra service
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div/input')).sendKeys('3')
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div/div[1]/div/nav/div/span[1]')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div/div[2]/button')).click()
)
.then(
    _ => driver.sleep(4000)
)
//switch back to generale tab
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/header/div/div[2]/div[2]/div/button[1]')).click()
)
.then(
    _ => driver.sleep(2000)
)
//this is to save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(2000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(5000)
)
.then(function() {
    if (( _=> driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[1]/div/div/div[2]/nav/div/div/span'))) !==null) {
        console.log('SUCCESSO');
    } else {
        console.log('FALLIMENTO');
    }
})
.then(
    _ => driver.sleep(5000)
)
.then(
    _ => driver.quit()
)

