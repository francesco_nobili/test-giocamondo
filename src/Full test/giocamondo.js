//Questo test prova i campi required di tutta l'applicazione di giocagest
//senza tener conto delle anagrafiche!!!!
//Francesco
var webdriver = require('selenium-webdriver');
const By = require('selenium-webdriver');
// Input capabilities
var capabilities = {
 'browserName' : 'Chrome',
 'browser_version' : '62.0',
 'os' : 'Windows',
 'os_version' : '10',
 'resolution' : '1600x1200',
 'browserstack.user' : 'francesco257',
 'browserstack.key' : 'iRBzCSYW6JzpqxsXwMnm',
 'browserstack.local' :true,
 'project' : 'test',
 'build' : 'Test Completo',
 'name' : 'giocagest',
}

var ctx = {}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

driver.session_

.then(
    sessionData => ctx.sessionId = sessionData.id_
)
.then(
    _ => driver.get('http://localhost:3000/#/')
)
.then(
    console.log('localost!')
)
.then(
    driver.sleep(3000)
)
//this is to full screen 
.then(
    driver.manage().window().setRect({
        x:0,
        y:0,
        width: 1600,
        height: 1200
    })
)
.then(
    _ => driver.sleep(1500)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div/div[3]/div[2]/div/button')).click()
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div/div[3]/div[2]/div/div/button')).click()
)
.then(
    _ => driver.sleep(3000)
)
//create new service
.then(
    console.log('create a new service')
)
//find & input service's code
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & input description
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[4]/div/textarea')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//next step is pubblicato
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[2]/label/span[1]/span[1]/span[1]/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
//time to change to "specifiche"
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[2]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//next step is to find Tipologia
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[1]/div/div/div')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[6]/div[2]/ul/li[1]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//find voucher
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[3]/div/div/div')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[6]/div[2]/ul/li[1]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//find & typing something into indirizzo
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[2]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & typing something into città
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[2]/div[2]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//time to change into Peridi 
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[3]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//click the button
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/button')).click()
)
.then(
    _ => driver.sleep(3000)
)
//insert data 1
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/div[1]/div[1]/div/input')).sendKeys(10102019)
)
.then(
    _ => driver.sleep(1000)
)
//insert data 2
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/div[1]/div[2]/div/input')).sendKeys(10202019)
)
.then(
    _ => driver.sleep(1000)
)
//insert tipologia prezzo
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/div[2]/div[1]/div/div/div/div')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[6]/div[2]/ul/li[2]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//time to change into Relazioni
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[4]')).click()
)
.then(
    _ => driver.sleep(1000)
)
//add tags
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[1]/div/div/span[1]/div[1]')).click()
)
.then(
    _ => driver.sleep(3000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div[1]/div/div/span[1]/div[2]/input')).sendKeys("la mia e")
)
.then(
    _ => driver.sleep(3000)
)
.then(
    _ => driver.findElement(webdriver.By.id('react-select-2--option-0')).click()
)
.then(
    _ => driver.sleep(1000)
)
//this is to save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(4000)
)
//switch to nucleo base
.then (
    _ => driver.get('http://localhost:3000/#/nucleo-base/new')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's code
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's nome
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[2]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's descrizione
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[1]/div[3]/div/textarea')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//insert data 1
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[2]/div[1]/div/input')).sendKeys(10102019)
)
.then(
    _ => driver.sleep(1000)
)
//insert data 2
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[2]/div[2]/div/input')).sendKeys(10202019)
)
.then(
    _ => driver.sleep(1000)
)
//next step is pubblicato
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/form/div/div[5]/div/label/span[1]/span[1]/span[1]/input')).click()
)
.then(
    _ => driver.sleep(1000)
)
//time to change to "servizi"
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/header/div/div[2]/div[2]/div/button[2]')).click()
)
.then(
    _ => driver.sleep(2000)
)
//time to search my servizio
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/div[2]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(2000)
)
//time to change to select my servizio
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/span/span[1]/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//add my servizio into my nucleo base
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[3]/div[1]/div/div/button')).click()
)
.then(
    _ => driver.sleep(2000)
)
//this is to save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(10000)
)
//switch to nucleo base
.then (
    _ => driver.get('http://localhost:3000/#/pacchetti/new')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's code
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[1]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's nome
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[1]/div[2]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//find & input nucleo base's descrizione
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[1]/div[3]/div/textarea')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(1000)
)
//insert data 1
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[2]/div[1]/div/input')).sendKeys(10102019)
)
.then(
    _ => driver.sleep(1000)
)
//insert data 2
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[2]/div[2]/div/input')).sendKeys(10202019)
)
.then(
    _ => driver.sleep(1000)
)
//next step is pubblicato
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/form/div/div[8]/div/label/span[1]/span[1]/span[1]/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//time to change to "servizi"
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/header/div/div[2]/div[2]/div/button[2]')).click()
)
.then(
    _ => driver.sleep(2000)
)
//time to search my nucleo base
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div/div[1]/div[1]/div/input')).sendKeys('Type something')
)
.then(
    _ => driver.sleep(2000)
)
//time to change to select my nucleo base
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div/div[3]/div[2]/table/tbody/tr/td[1]/span/span[1]/input')).click()
)
.then(
    _ => driver.sleep(2000)
)
//add my servizio into my nucleo base
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div/button')).click()
)
.then(
    _ => driver.sleep(2000)
)
//this is to save
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/button')).click()
)
.then(
    _ => driver.sleep(1000)
)
.then(
    _ => driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[2]/div/div/button[2]')).click()
)
.then(
    _ => driver.sleep(10000)
)
//function
.then(function() {
    if (( _=> driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div/div[1]/div[1]/div/div/div[2]/nav/div/div/span'))) !==null) {
        console.log('SUCCESSO');
    } else {
        console.log('FALLIMENTO');
    }
})
//quit
.then(
    _ => driver.sleep(4000)
)
.then(
    _ => driver.quit()
)